import java.util.LinkedList;

import org.opencv.core.Mat;

/* Each node represents a state of the images which is the output
 * of a sequence of operations.
 * Includes the parent node, the operation that leads to this node
 * from it parent and a list of children nodes (corresponding to all
 * possible operations from this state).
 * The node has a level in the tree
 *  nbStateSelect, nbActionSelect and rewardActionSelect store the
 *  information from previous simulations (see select class). Each cell
 *  of the table corresponds to the filter with the same number.
 */
@SuppressWarnings("unused")
abstract class Node {

	public Node parent;
	public LinkedList<Node> children;
	public LinkedList<Edge> childrenEdges;
	public int level;
	public int nbStateSelect;
	public int[] nbActionSelect;
	public double[] rewardActionSelect;
	public Operation op;
	public OperationsBank operationsBank;
	public int depthMax;


	abstract Node copy();

	public void setOperationsBank(OperationsBank operationsBank){
		this.operationsBank = operationsBank;
		this.nbActionSelect = new int[this.operationsBank.getSize()];
		this.rewardActionSelect = new double[this.operationsBank.getSize()];
	}

	public void setParent(Node parent){
		this.parent = parent;
	}

	public void setLevel(int level){
		this.level = level;
	}

	abstract void addChild(Node node, Operation operation);

	public void addChildEdge(Node child, Operation op){

		Edge edge = new Edge();

		edge.setParent(this);
		edge.setChild(child);
		edge.setOperation(op);

		this.childrenEdges.add(edge);
	}


	public void incrementNbActionSelect(Operation op){
		this.nbActionSelect[op.num-1] = this.nbActionSelect[op.num-1] + 1;

	}

	public void setRewardActionSelect(Operation op, double reward){
		this.rewardActionSelect[op.num-1] = this.rewardActionSelect[op.num-1] + reward;
	}

	public void incrementNbStateSelect(){
		this.nbStateSelect++;
	}

	public void update(Operation op, double reward){
		setRewardActionSelect(op, reward); // s(x,u)
		incrementNbActionSelect(op); // n(x,u)
		incrementNbStateSelect(); // n(x)
	}
	/* Given an operation, returns the corresponding child
	 * returns null if the child does not exist.
	 */
	public Node OperationFindChild(Operation op){

		int k = this.childrenEdges.size();
		Edge edge = new Edge();

		for(int i = 0; i < k; i++){
			edge = this.childrenEdges.get(i);

			if(edge.op.num == op.num){
				return edge.child;
			}
		}
		System.out.println("Child does not exist!");
		return null;
	}

	/* Given a child, returns the corresponding operation
	 * returns null if the child does not exist.
	 */
	public Operation childFindOperation(Node child){

		int k = this.childrenEdges.size();
		Edge edge = new Edge();

		for(int i = 0; i < k; i++){
			edge = this.childrenEdges.get(i);

			if(edge.child.equals(child)){
				return edge.op;
			}
		}
		System.out.println("Operation does not exist!");
		return null;
	}
	/*
	public boolean opUsed (Operation op){
		Node child = new Node();
		for (int i = 0; i < this.children.size(); i++){
			child = this.children.get(i);
			if(this.childFindOperation(child).num == op.num){
				return true;
			}
		}
		return false;
	}
	 */
	abstract void expend();

	/* Check if the node has been extended or not
	 * 
	 */
	public boolean hasChildren(){
		if( this.children == null){
			return false;			
		}else if(this.children.size() == 0){
			return false;
		}else{
			return true;
		}
	}

	abstract void display();

	abstract void displayFamily();

}
