import java.util.LinkedList;

import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

public class ImgProcImage {

	public String imgAdr;
	public String labelImgAdr;
	public String state;
	public LinkedList<Operation> bestFiltersSeq;
	public Path bestPath;
	public double bestReward;
	
	public ImgProcImage(){
		this.imgAdr = "";
		this.labelImgAdr = "";
		this.state = "Grey";
		this.bestFiltersSeq = new LinkedList<Operation>();
		this.bestReward = 0.0;
		this.bestPath = new Path();
	}
	
	public void display(){
		
		Mat inputImg = Highgui.imread(this.imgAdr);
		Imgproc.cvtColor(inputImg, inputImg, Imgproc.COLOR_BGR2GRAY);//convert input image to gray scale

		Mat labeledImg = Highgui.imread(this.labelImgAdr);
		Imgproc.cvtColor(labeledImg, labeledImg, Imgproc.COLOR_BGR2GRAY);//convert input image to gray scale
		
		Mat outputImg = new Mat();
		for(int i = 0; i < this.bestPath.bestBranch.size(); i++){
			System.out.println(this.bestPath.bestBranch.get(i).num);			
		}
		LinkedList<Integer> seq = new LinkedList<Integer>();
		for(int i = 0; i < this.bestPath.bestBranch.size(); i++){
			seq.add(this.bestPath.bestBranch.get(i).num);
		}
		System.out.println("reward = " + this.bestPath.bestReward);
		FiltersBank.runFilters(seq, inputImg, outputImg);
		FiltersBank.displayImage(labeledImg, "Labeled image");
		FiltersBank.displayImage(outputImg, "Output image");
	}
	
	public void save(String address){
		Mat inputImg = Highgui.imread(this.imgAdr);
		Imgproc.cvtColor(inputImg, inputImg, Imgproc.COLOR_BGR2GRAY);//convert input image to gray scale
		Mat outputImg = new Mat();
		LinkedList<Integer> seq = new LinkedList<Integer>();
		for(int i = 0; i < this.bestPath.bestBranch.size(); i++){
			seq.add(this.bestPath.bestBranch.get(i).num);
		}
		FiltersBank.runFilters(seq, inputImg, outputImg);
		Highgui.imwrite(address, outputImg);
	}
}
