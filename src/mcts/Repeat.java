import java.util.LinkedList;

/* Repeats the sub-search component nbRepetitions times
 * 
 */
public class Repeat extends SearchComponent{

	public int nbRepetitions;
	public SearchComponent searchComp;

	public Repeat(){
		super();
		setNbRepetitions(0);
		setSearchComponent(null);
		this.name = "Repeat";
	}

	public void setNbRepetitions(int n){
		this.nbRepetitions = n;
	}

	public void setSearchComponent(SearchComponent searchComp){
		this.searchComp = searchComp;
	}

	public int getNbRepetitions(){
		return this.nbRepetitions;
	}

	public SearchComponent getSearchComponent(){
		return this.searchComp;
	}

	public void apply(LinkedList<Operation> sequence, Node state){
		//System.out.println("Repeat");
		for (int i = 0; i < this.nbRepetitions; i++){
			if(this.searchComp.name.equals("Select")){
				this.param.setRepeat2(i);
			}else{
				this.param.setRepeat1(i);
			}
			invoke(this.searchComp, sequence, state);
		}
	}
}
