import java.util.LinkedList;

/* Simulate performs simulations according to the simulate policy
 * starting from a given state
 * until reaching a final state.
 */
public class Simulate extends SearchComponent{

	public SimPolicy simPolicy;

	public Simulate(){

		super();
		setSimPolicy(null);
		this.name = "Simulate";
	}

	public void setSimPolicy(SimPolicy simPolicy){
		this.simPolicy = simPolicy;
	}

	public SimPolicy getSimPolicy(){
		return this.simPolicy;
	}

	@SuppressWarnings("unchecked")
	public void apply(LinkedList<Operation> sequence, Node state){
		//System.out.println("Simulate!");

		int l = state.level;
		Operation op;
		Node nextState;
		LinkedList<Operation> seq = new LinkedList<Operation>();
		nextState = state.copy();
		
		seq = (LinkedList<Operation>) sequence.clone();

		for (int t = l ; t < depth ; t++){
			
			this.param.setSimulate(t);
			
			if((seq.size() != 0) && (seq.getLast().name.equals("Stop"))){
				break;
			}
			
			op = this.simPolicy.apply(nextState);
			
			seq.add(op);
			nextState = nextState.OperationFindChild(op);
		}

		yield(seq);
	}
}
