import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import de.nixosoft.jlr.JLRConverter;
/* Includes parameters for the image processing problem
 * All the learning and test, original and labelled images
 * Addresses for images
 * All the filters used and there parameters
 * The dimensions of the images used as learning set
 */
public class ImgProcParameters extends Parameter{

	int operationsBankSize;

	ArrayList<Integer> filtersNumber;
	ArrayList<String> filtersNames;
	ArrayList<Integer> filtersLambda;
	ArrayList<Integer> filtersSigma;
	ArrayList<Integer> filtersSize;

	ArrayList<Mat> originalImg;
	ArrayList<Mat> labeledImg;
	ArrayList<Mat> testOriginalImg;
	ArrayList<Mat> testLabeledImg;
	ArrayList<Mat> resultImg;

	LinkedList<String> imgNames;
	LinkedList<String> testImgNames;
	String originalImgAddress;
	String labeledImgAddress;
	String testOriginalImgAddress;
	String testLabeledImgAddress;
	
	int topX, topY, width, height;

	@SuppressWarnings("unused")
	public ImgProcParameters(){

		String stringVal = "";
		char charVal = ' ';
		int intVal = 0;
		double doubleVal = 0.0;
		boolean booleanVal = true;

		depth = -1;
		operationsBankSize = -1;
		budget = -1;
		nbRepetitions1 = -1;
		nbRepetitions2 = -1;
		hyperParam = -1;

		time = 0;
		h = 0;
		m = 0;
		s = 0;
		d = 0;

		step = 0;
		repeat1 = 0;
		repeat2 = 0;
		lookahead = 0;
		select = 0;
		simulate = 0;
		outOfBudget = false;

		filtersNumber = new ArrayList<Integer>();
		filtersNames = new ArrayList<String>();
		filtersLambda = new ArrayList<Integer>();
		filtersSigma = new ArrayList<Integer>();
		filtersSize = new ArrayList<Integer>();

		selectPolicy = "";
		simPolicy = "";
		ram = 0.0;
		processor = "";
		os = "";
		javaVersion = 0.0;

		bestPath = new Path();

		reportEdited = false;
		runComplited = false;
		computationCompleted = false;

		originalImg = new ArrayList<Mat>();
		labeledImg = new ArrayList<Mat>();
		testOriginalImg = new ArrayList<Mat>();
		testLabeledImg = new ArrayList<Mat>();
		resultImg = new ArrayList<Mat>();
		
		this.imgNames = new LinkedList<String>();
		this.testImgNames = new LinkedList<String>();

		originalImgAddress = "";
		labeledImgAddress = "";
		testOriginalImgAddress = "";
		testLabeledImgAddress = "";
		this.reportAdr = "";
		mctsTemplate = "";

		this.seed = 0;
		budgetUsed = 0;
		
		this.topX = 0;
		this.topY = 0;
		this.width = 0;
		this.height = 0;
	}

	public void setLearningImgSize(int x, int y, int width, int height){
		this.topX = x;
		this.topY = y;
		this.width = width;
		this.height = height;
	}

	public void setOriginalAddress(String address){
		this.originalImgAddress = address;
	}

	public void setLabeledImgAddress(String address){
		this.labeledImgAddress = address;
	}
	
	public void setTestOriginalAddress(String address){
		this.testOriginalImgAddress = address;
	}

	public void setTestLabeledImgAddress(String address){
		this.testLabeledImgAddress = address;
	}

	public void addOriginalImg(Mat img){
		this.originalImg.add(img);
	}

	public void addLabeledImg(Mat img){
		this.labeledImg.add(img);
	}
	
	public void addTestOriginalImg(Mat img){
		this.testOriginalImg.add(img);
	}

	public void addTestLabeledImg(Mat img){
		this.testLabeledImg.add(img);
	}

	public void addFilter(int number, String name, int lambda, int sigma, int size){
		this.filtersNumber.add(number);
		this.filtersNames.add(name);
		this.filtersLambda.add(lambda);
		this.filtersSigma.add(sigma);
		this.filtersSize.add(size);
		this.operationsBankSize++;
	}

	public void setOperationsBankSize(int size){
		this.operationsBankSize = size;
	}
	/* Calculates an approximative progression percentage and displays it.
	 * The percentage is calculated based on the maximum operations possible
	 * during one run.
	 * (non-Javadoc)
	 * @see Parameter#displayProgress()
	 */
	public void displayProgress(){

		int total = (depth-1)*nbRepetitions1*nbRepetitions2*operationsBankSize;
		int progress; 
		progress = repeat1;
		progress = progress + nbRepetitions1*lookahead;
		progress = progress + nbRepetitions1*operationsBankSize*repeat2;
		progress = progress + nbRepetitions1*operationsBankSize*nbRepetitions2*step;

		progress = (progress*100)/total;

		System.out.println("Progress "+progress+" %");			
	}

	/* Edit a txt report.
	 * Creates a new folder to include the report and result images
	 * Creates a txt report and fill it with all the information 
	 * related to the run
	 */
	@SuppressWarnings("deprecation")
	public void editReport(){
		if(this.reportEdited){
			System.out.println("Report already edited!");
		}else{
			System.out.println((char)27 + "[33mEditing report");
			BufferedWriter out;
			File repo = new File(this.reportAdr);
			String dirAdr = this.reportAdr + "/Report"+Integer.toString(repo.list().length+1);
			File directory = new File(dirAdr);
			if (!directory.exists()) {
				if (directory.mkdir()) {
					System.out.println((char)27 + "[32mDirectory is created");
				} else {
					System.out.println("Failed to create directory!");
				}
			}
			String name = dirAdr+"/report.txt";
			//File report = new File(name);
			try {

				// 1) Instanciation de l'objet
				File report = new File(name);
				out = new BufferedWriter(new FileWriter(report));

				try {

					// 2) Utilisation de l'objet
					out.write("Computation:");
					out.newLine();
					out.write("Start time = "+ start.getDate()+"/"+Integer.toString(start.getMonth()+1)+"/"+Integer.toString(start.getYear()+1900)+" "+start.getHours()+":"+start.getMinutes());
					out.newLine();
					out.write("End time = "+ end.getDate()+"/"+Integer.toString(end.getMonth()+1)+"/"+Integer.toString(end.getYear()+1900)+" "+end.getHours()+":"+end.getMinutes());
					out.newLine();
					out.write("The hole process took "+Integer.toString(d)+"days "+Integer.toString(h)+"h "+Integer.toString(m)+"min "+Integer.toString(s)+"s");
					out.newLine();
					out.newLine();
					out.write("System features :");
					out.newLine();
					out.write("Operating system = "+this.os);
					out.newLine();
					out.write("Memory = "+Double.toString(this.ram)+" GiB");
					out.newLine();
					out.write("Processor = "+this.processor);
					out.newLine();
					out.write("Java version = "+Double.toString(this.javaVersion));
					out.newLine();
					out.newLine();
					out.write("Number of images used = "+Integer.toString(this.originalImg.size()));
					out.newLine();
					out.newLine();
					out.write("Algorithm used : ");
					out.write("Step(Repeat(N2, Select(Lookahead(Repeat(N1, Simulate)))))");
					out.newLine();
					out.write("N1 = "+Integer.toString(nbRepetitions1));
					out.newLine();
					out.write("N2 = "+Integer.toString(nbRepetitions2));
					out.newLine();
					out.newLine();
					out.write("Maximum budget = "+Integer.toString(budget));
					out.newLine();
					out.write("Used budget = "+Integer.toString(budgetUsed));
					out.newLine();
					out.newLine();
					out.write("Select policy : "+selectPolicy);
					out.newLine();
					out.write("Hyper parameter = "+Double.toString(hyperParam));
					out.newLine();
					out.newLine();
					out.write("Simulate policy : "+simPolicy);
					out.newLine();
					out.write("Seed : "+this.seed);
					out.newLine();
					out.newLine();
					out.write("Max depth = "+Integer.toString(depth));
					out.newLine();
					out.newLine();
					if(this.computationCompleted){
						out.write("Program run completely!");
					}else{
						out.write("Program run half way!");
						out.newLine();
						out.newLine();
						if(this.outOfBudget){
							out.write("Program run out of budget!");
							out.newLine();
							out.newLine();
						}
						out.write("Details:");
						out.newLine();
						out.newLine();
						out.write("Step = "+Integer.toString(step));
						out.newLine();
						out.write("Repeat1 = "+Integer.toString(repeat1));
						out.newLine();
						out.write("Select = "+Integer.toString(select));
						out.newLine();
						out.write("Lookahead = "+Integer.toString(lookahead));
						out.newLine();
						out.write("Repeat2 = "+Integer.toString(repeat2));
						out.newLine();
						out.write("Simulate = "+Integer.toString(simulate));
					}

					out.newLine();
					out.newLine();
					out.write("Rewards :");
					out.newLine();
					for(int i = 0; i < bestPath.bestRewards.size(); i++){
						out.write("Image " + Integer.toString(i+1) + " : " + Double.toString(bestPath.bestRewards.get(i)));
						out.newLine();
					}
					out.write("Best reward : " + Double.toString(bestPath.bestReward));
					out.newLine();
					out.newLine();
					out.write("Filters bank : ");
					out.newLine();
					Operation op;
					for(int i = 0; i < operationsBankSize; i++){
						out.write("F" + Integer.toString(filtersNumber.get(i)) + " : " + filtersNames.get(i)+"(");
						if(filtersLambda.get(i)>0){
							out.write("( lambda = "+Integer.toString(filtersLambda.get(i)));
						}
						if(filtersSigma.get(i)>0){
							out.write(" sigma = "+Integer.toString(filtersSigma.get(i)));
						}
						if(filtersSize.get(i)>0){
							out.write(" size = "+Integer.toString(filtersSize.get(i)));
						}
						out.write(" )");
						out.newLine();
					}
					out.newLine();
					out.write("Best sequence : ");
					out.newLine();
					for(int i = 0; i < bestPath.bestBranch.size(); i++){
						op = bestPath.bestBranch.get(i);
						out.write("F" + Integer.toString(op.num) + " : " + op.name);
						out.newLine();
					}
				} finally {

					// 3) Libération de la ressource exploitée par l'objet
					out.close();

				}

			} catch (IOException e) {
				e.printStackTrace();
			}

			this.reportEdited = true;

			this.editResults(dirAdr, false);
		}
	}

	/* Edit a LateX template that can be used to build a pdf report.
	 * Creates a new folder to include the final template and result images
	 * Uses a template to generate apdf report including all the information 
	 * related to the run
	 */
	@SuppressWarnings("deprecation")
	public void editLateXReport(){

		if(this.reportEdited){

			System.out.println((char)27 + "[34mReport already edited!");

		}else{

			System.out.println((char)27 + "[33mEditing report");
			File repo = new File(this.reportAdr);
			int repoSize = repo.list().length;
			String dirAdr = this.reportAdr + "/Report"+Integer.toString(repoSize+1);
			File directory = new File(dirAdr);
			

			String name = dirAdr+"/report.tex";
			File report = new File(name);
			File template = new File(this.mctsTemplate);
	
			if (!directory.exists()) {
				if (directory.mkdir()) {
					System.out.println((char)27 + "[32mDirectory Report"+Integer.toString(repoSize+1)+" created");
				} else {
					System.out.println("Failed to create directory!");
				}
			}else{
				System.out.println((char)27 + "[32mDirectory Report"+Integer.toString(repoSize+1)+" edited");
			}

			try {
				JLRConverter converter = new JLRConverter(repo);

				Operation op;
				LinkedList<LinkedList<String>> operations = new LinkedList<LinkedList<String>>();
				for(int i = 0; i < operationsBankSize; i++){
					LinkedList<String> operation = new LinkedList<String>();
					operation.add(Integer.toString(filtersNumber.get(i)));
					operation.add(filtersNames.get(i));

					if(filtersLambda.get(i)>0){
						operation.add(Integer.toString(filtersLambda.get(i)));
					}else{
						operation.add("-");
					}
					if(filtersSigma.get(i)>0){
						operation.add(Integer.toString(filtersSigma.get(i)));
					}else{
						operation.add("-");
					}
					if(filtersSize.get(i)>0){
						operation.add(Integer.toString(filtersSize.get(i)));
					}else{
						operation.add("-");
					}
					operations.add(operation);
				}

				converter.replace("OPERATIONS", operations);
				converter.replace("NBIMAGES", Double.toString(this.originalImg.size()));
				converter.replace("MEMORY", this.ram);
				converter.replace("CPU", this.processor);
				converter.replace("OS", this.os);
				converter.replace("JAVA_VERSION", Double.toString(this.javaVersion));
				converter.replace("ALGORITHM", "Step(Repeat(N2, Select(Lookahead(Repeat(N1, Simulate)))))");
				converter.replace("NB1", Integer.toString(nbRepetitions1));
				converter.replace("NB2", Integer.toString(nbRepetitions2));
				converter.replace("MAX_BUDGET", Integer.toString(budget));
				converter.replace("USED_BUDGET", Integer.toString(budgetUsed));
				converter.replace("SELECT_POLICY", selectPolicy);
				converter.replace("SIMPOLICY", simPolicy);
				converter.replace("SEED", this.seed);
				converter.replace("HYPER_PARAM", Double.toString(hyperParam));
				converter.replace("DEPTH", Integer.toString(depth));
				converter.replace("TOPX", Integer.toString(topX));
				converter.replace("TOPY", Integer.toString(topY));
				converter.replace("WIDTH", Integer.toString(width));
				converter.replace("HEIGHT", Integer.toString(height));

				if(this.computationCompleted){
					converter.replace("RUN_COMPLETED", "Program run completely!");
					converter.replace("OUT_OF_BUDGET", " ");
				}else{
					converter.replace("RUN_COMPLETED", "Program run half way!");
					if(this.outOfBudget){
						converter.replace("OUT_OF_BUDGET", "Program run out of budget!");
					}else{
						converter.replace("OUT_OF_BUDGET", " ");
					}
				}

				converter.replace("STEP", Integer.toString(step));
				converter.replace("REPEAT_ONE", Integer.toString(repeat1));
				converter.replace("SELECT", Integer.toString(select));
				converter.replace("LOOKAHEAD", Integer.toString(lookahead));
				converter.replace("REPEAT_TWO", Integer.toString(repeat2));
				converter.replace("SIMULATE", Integer.toString(simulate));

				LinkedList<LinkedList<String>> rewards = new LinkedList<LinkedList<String>>();
				for(int i = 0; i < bestPath.bestRewards.size(); i++){
					LinkedList<String> subRewards = new LinkedList<String>();
					subRewards.add(this.imgNames.get(i));
					subRewards.add(Double.toString(bestPath.bestRewards.get(i)));
					rewards.add(subRewards);
				}
				
				converter.replace("REWARDS", rewards);
				converter.replace("REWARD", Double.toString(bestPath.bestReward));

				LinkedList<LinkedList<String>> services = new LinkedList<LinkedList<String>>();
				for(int i = 0; i < bestPath.bestBranch.size(); i++){
					LinkedList<String> subservice = new LinkedList<String>();
					op = bestPath.bestBranch.get(i);
					subservice.add(Integer.toString(op.num));
					subservice.add(op.name);
					services.add(subservice);
				}
				converter.replace("SERVICES", services);
				converter.replace("START_DATE", Integer.toString(start.getDate())+"/"+Integer.toString(start.getMonth()+1)+"/"+Integer.toString(start.getYear()+1900));
				converter.replace("START_TIME", Integer.toString(start.getHours())+":"+Integer.toString(start.getMinutes()));
				converter.replace("END_DATE", Integer.toString(end.getDate())+"/"+Integer.toString(end.getMonth()+1)+"/"+Integer.toString(end.getYear()+1900));
				converter.replace("END_TIME", Integer.toString(end.getHours())+":"+Integer.toString(end.getMinutes()));
				converter.replace("DAYS", Integer.toString(d));
				converter.replace("HOURS", Integer.toString(h));
				converter.replace("MINUTES", Integer.toString(m));
				converter.replace("SECONDS", Integer.toString(s));

				LinkedList<LinkedList<String>> learningOutputs = this.editResults(dirAdr,false);
				LinkedList<LinkedList<String>> testOutputs = this.editResults(dirAdr,true);

				converter.replace("OUTPUTS_ADDRESS", dirAdr+"/");
				converter.replace("LEARNING_OUTPUTS", learningOutputs);
				converter.replace("TEST_OUTPUTS", testOutputs);
				
				converter.parse(template, report);

			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}
	/* Edit the final images including the original, the hand labelled and the machine
	 * labelled images in a column.
	 *  (non-Javadoc)
	 * @see Parameter#editResults(java.lang.String, boolean)
	 */
	public LinkedList<LinkedList<String>> editResults(String dirAdr, boolean test){

		//System.out.println((char)27 + "[33mEditing results...");
		
		int size = 0;
		String originalAddress = null;
		String labeledAddress = null;
		String extension = null;
		LinkedList<String> names = new LinkedList<String>();
		
		if(test){
			System.out.println((char)27 + "[33mEditing test set results...");
			size = this.testOriginalImg.size();
			originalAddress = this.testOriginalImgAddress;
			labeledAddress = this.testLabeledImgAddress;
			names = this.testImgNames;
			extension = "test";
		}else{
			System.out.println((char)27 + "[33mEditing learning set results...");
			size = this.originalImg.size();
			originalAddress = this.originalImgAddress;
			labeledAddress = this.labeledImgAddress;
			names = this.imgNames;
			extension = "learning";
		}
		
		String name = "";

		Mat im1 = new Mat();
		Mat im2 = new Mat();
		Mat im3 = new Mat();
		Mat im = new Mat();
		LinkedList<LinkedList<String>> output = new LinkedList<LinkedList<String>>();

		double jaccard = 0.0;

		LinkedList<Integer> seq = new LinkedList<Integer>();
		for(int j = 0; j < this.bestPath.bestBranch.size(); j++){
			seq.add(this.bestPath.bestBranch.get(j).num);
		}

		LinkedList<String> list = new LinkedList<String>();

		for(int k = 0; k < size; k++){

			im1  = Highgui.imread(originalAddress+"/"+names.get(k)+".JPG");
			im2  = Highgui.imread(labeledAddress+"/"+names.get(k)+"-labeled.JPG");
			name = names.get(k);
			
			Imgproc.cvtColor(im1, im, Imgproc.COLOR_BGR2GRAY);
			int success = FiltersBank.runFilters(seq, im, im3);//run the list of filters
			if (success==0)
				System.out.println("Running the array of filters failed at one of the filters.");

			jaccard = ImgProcReward.jaccardScore(im3, im2);

			Highgui.imwrite(dirAdr+"/"+name+"-"+extension+".JPG", im3);
			im3 = Highgui.imread(dirAdr+"/"+name+"-"+extension+".JPG");
			Size sz1 = im1.size();
			Size sz2 = im2.size();
			Size sz3 = im3.size();

			int maxWidth = (int) sz1.width;
			if(maxWidth<(int)sz3.width){
				maxWidth = (int) sz3.width;
			}
			if(maxWidth<(int)sz2.width){
				maxWidth = (int) sz2.width;
			}

			Rect rec1 = new Rect(25,25,(int)sz1.width,(int)sz1.height);
			Rect rec2 = new Rect( 25,(int)sz1.height+50, (int)sz2.width, (int)sz2.height);
			Rect rec3 = new Rect( 25,(int)(sz1.height+sz2.height)+75, (int)sz3.width, (int)sz3.height);

			Mat im7 = new Mat((int)(sz1.height+sz2.height+sz3.height)+100, maxWidth+50, CvType.CV_8UC3);

			Mat left = new Mat(im7, rec1);
			im1.copyTo(left);
			Mat centre = new Mat(im7, rec2);
			im2.copyTo(centre);
			Mat right = new Mat(im7, rec3);
			im3.copyTo(right);

			//FiltersBank.displayImage(im7, "Test param");
			Highgui.imwrite(dirAdr+"/"+name+"-"+extension+".JPG", im7);
			list.add(name+"-"+extension);
			list.add(name);
			list.add(Double.toString(jaccard));
			if(list.size() == 9){
				output.add(list);
				list = new LinkedList<String>();
			}			
		}	
		return output;
	}
}