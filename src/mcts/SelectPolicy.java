/* UCB-1 select policy
 * If an operation has never been explored, it is selected automatically
 * Otherwise, the operation selected is the one returned with the UCB-1 policy
 */
public class SelectPolicy{

	public OperationsBank operationsBank;
	public double hyperParameter;

	public SelectPolicy(){

		setOperationsBank(null);
		setHyperParameter(0);
	}

	public void setOperationsBank(OperationsBank operationsBank2){
		this.operationsBank = operationsBank2;
	}

	public void setHyperParameter(double c){
		this.hyperParameter = c;
	}

	public OperationsBank getOperationBank(){
		return this.operationsBank;
	}

	public double getHyperParameter(){
		return this.hyperParameter;
	}

	public Operation UCB_1(Node state){

		int l = state.children.size();
		int bestOperation = 0;
		double arg;
		double argmax = 0.0;
		double s;
		int n,N;
		Operation op;

		for (int i = 0; i < l; i++ ){
			
			op = state.children.get(i).op;
			s = state.rewardActionSelect[op.num - 1];
			n = state.nbActionSelect[op.num - 1];
			N = state.nbStateSelect;

			if (n == 0){
				return state.children.get(i).op;
			}else{

				arg = Math.log(N) / n;
				arg = Math.sqrt(arg);		
				arg = arg * this.hyperParameter;
				arg = arg + (s/n);

				if(arg > argmax){
					argmax = arg;
					bestOperation = i;
				}
			}
		}
		
		return state.children.get(bestOperation).op;
	}
}
