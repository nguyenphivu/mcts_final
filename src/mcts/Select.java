import java.util.LinkedList;

/* Select search component:
 * Stores statistics on the outcomes of sub-searches
 * Uses these information to bias sub-searches selection
 * Has 3 steps: select, sub-search and back-propagation
 */
public class Select extends SearchComponent{

	public SelectPolicy selectPolicy;
	public SearchComponent searchComp;

	public Select(){
		super();
		setSearchComponent(null);
		setSelectPolicy(null);
		this.name = "Select";
	}

	public void setSearchComponent(SearchComponent searchComp){
		this.searchComp = searchComp;
	}

	public void setSelectPolicy(SelectPolicy selectPolicy){
		this.selectPolicy = selectPolicy;
	}

	public SearchComponent getSearchComponent(){
		return this.searchComp;
	}

	public SelectPolicy getSelectPolicy(){
		return this.selectPolicy;
	}

	@SuppressWarnings("unchecked")
	public void apply(LinkedList<Operation> sequence, Node state){

		//System.out.println("Select!");
		
		int t = state.level;

		Operation op;
		Node nextState;
		LinkedList<Operation> nextSeq = new LinkedList<Operation>();
		LinkedList<Operation> seq = new LinkedList<Operation>();
		Node prevState;
		Node node;

		nextSeq = (LinkedList<Operation>) sequence.clone();

		nextState = state;	

		if((nextSeq.size() != 0) && (nextSeq.getLast().name.equals("Stop"))){
			invoke(this.searchComp, nextSeq, nextState);
			return;
		}

		for(int i = t; i < depth; i++){

			this.param.setSelect(i);

			if(nextState.hasChildren() == false){
				nextState.expend();
			}
			
			// Select using UCB-1
			op = this.selectPolicy.UCB_1(nextState);
			nextSeq.add(op);
			nextState = nextState.OperationFindChild(op);

			if(nextState.nbStateSelect == 0){
				break;
			}
		}

		seq = (LinkedList<Operation>) nextSeq.clone();

		node = nextState.copy();
		invoke(this.searchComp, seq, node); // Sub-search
		prevState = nextState;

		// Back propagation
		while(prevState.parent != null){
			op = prevState.op;
			prevState.nbStateSelect++;
			prevState = prevState.parent;
			prevState.nbActionSelect[op.num - 1] = prevState.nbActionSelect[op.num - 1] + 1;
			prevState.rewardActionSelect[op.num - 1] = prevState.rewardActionSelect[op.num - 1] + this.bestPath.bestReward;
		}
		prevState.nbStateSelect++;
	}
}
