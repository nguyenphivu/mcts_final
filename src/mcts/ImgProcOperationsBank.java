import java.util.LinkedList;

/* Includes all the filters that are used.
 * When a filter is added, it is sorted in a way that keeps
 * three groups of filters: grey scale, thresholding and binary.
 */
public class ImgProcOperationsBank extends OperationsBank{
	
	int nbGreyScale;
	int nbThreshold;
	int nbBinary;

	public ImgProcOperationsBank(){
		this.operations = new LinkedList<Operation>();
		this.nbGreyScale = 0;
		this.nbThreshold = 0;
		this.nbBinary = 0;
		
	}
	void addOperation(Operation op) {
		if(op.name.equals("Gabor") || op.name.equals("Gaussian")){
			this.operations.add(nbGreyScale, op);
			this.nbGreyScale++;
		}else if(op.name.equals("Thresholding")){
			this.operations.add(nbGreyScale+nbThreshold, op);
			this.nbThreshold++;
		}else if(op.name.equals("Opening") || op.name.equals("Closing")){
			this.operations.add(nbGreyScale+nbThreshold+nbBinary , op);
			this.nbBinary++;
		}else if(op.name.equals("Stop")){
			this.operations.add(op);
		}else{
			System.out.println("Operation not added");
		}
		
	}
	
	void addOperation(int num, String name){
		
		ImgProcOperation op = new ImgProcOperation();
		
		op.num = num;
		op.name = name;
		
		if(name.equals("Gabor") || name.equals("Gaussian")){
			this.operations.add(nbGreyScale, op);
			this.nbGreyScale++;
		}else if(name.equals("Thresholding")){
			this.operations.add(nbGreyScale+nbThreshold, op);
			this.nbThreshold++;
		}else if(name.equals("Opening") || name.equals("Closing")){
			this.operations.add(nbGreyScale+nbThreshold+nbBinary , op);
			this.nbBinary++;
		}else if(name.equals("Stop")){
			this.operations.add(op);
		}else{
			System.out.println("Operation not added");
		}
	}

	void display() {
		for(Operation op : this.operations){
			System.out.println("Num "+op.num+" : "+op.name);
		}
		System.out.println("Grey "+this.nbGreyScale+" Threshold "+this.nbThreshold+" Binary "+this.nbBinary);
	}
	
	

}
